


      PROGRAM JACOBI


      use omp_lib 
      !use openacc

      PARAMETER ( nx = 100000 )
      PARAMETER ( iter_max = 1000000/10, tol = 1.0e-6 )
          INTEGER broj_niti 

      dimension A(nx), Anew(nx)
      

      A(1) = 1.
    
      call cpu_time(time_begin)
!-------------------------------------------

      error = 0.
      iter  = 0


!$acc data copy(A, error,
!$acc* Anew, iter )

!$OMP PARALLEL
      
!     Main loop

      do it = 1, iter_max

      error = 0.
      iter  = iter + 1

!$acc kernels
!$OMP DO
      do i = 2, nx-1
        Anew(i) = 0.5 * ( A(i+1) + A(i-1) )
        error = amax1 ( error, abs( Anew(i) - A(i) ) )
      enddo
!$OMP END DO
!$acc end kernels

      Anew(nx) = Anew(nx-1)

!$acc kernels
!$OMP DO
      do i = 2, nx
        A(i) = Anew (i)
      enddo
!$OMP END DO
!$acc end kernels

      enddo
      
!$acc end data

!       PRINT *, "Nit broj:",omp_get_thread_num() 
!$OMP SINGLE
        PRINT *, "Broj niti",omp_get_num_threads()
!$OMP END SINGLE
       
!$OMP END PARALLEL

      call cpu_time(time_end)
      CPU = time_end - time_begin

      write(*,*)
      PRINT *, "CPU time",CPU
      write(*,*)
      PRINT *, "greska, br. iteracija, A(nx)", error, iter, A(nx)
      
!      PAUSE
      write(12,'(10f7.3)') A

!      PAUSE
      
      END
